import './App.css';
import ClickCounter from './components/clickCounter';
import ErrorBoundary from './components/errorBoundary';
import HoverCounter from './components/hoverCounter';

function App() {
  return (
    <ErrorBoundary>
      <div className='counter'>
        <ClickCounter />
      </div>

      <div>
        <HoverCounter />
      </div>
    </ErrorBoundary>
  );
}

export default App;
