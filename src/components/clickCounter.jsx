import React, { Component } from 'react';
import withCounter from './withCounter';
import '../App.css';

class ClickCounter extends Component {

  render() {
    const { count, incrementCount } = this.props
		return <button className='btn' role="button" onClick={incrementCount}>Clicked {count} times</button>
	}
}

export default withCounter(ClickCounter, 5);